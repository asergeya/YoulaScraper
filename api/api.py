import urllib.request
import json 
import csv


def reverse_string(str: str) -> str:
    return str[::-1]


def get_productID(url: str) -> str:
    url = reverse_string(url)
    productID = url[:url.index('-')]

    return productID[::-1]
    

def write_header(csv_file):
    writer = csv.writer(csv_file)
    writer.writerow(["Наименование", "Ссылка", "Просмотры", "В избранном", "Ссылка на продавца", "Активные", "Проданные"])


def write_row(csv_file, info):
    writer = csv.writer(csv_file)
    writer.writerow(info)


def get_product_info(productID):
    with urllib.request.urlopen(f"https://api.youla.io/api/v1/product/{productID}") as url:
        data = json.loads(url.read().decode())
        
        return data['data']


def get_owner_info(product_info):
    return product_info['owner']


def get_link_to_profile(userID):
    return f"https://youla.ru/user/{userID}"


def prepare_record(product_info):
    owner_info = get_owner_info(product_info)
    link_to_profile = get_link_to_profile(owner_info['id'])
    record = [
            product_info['name'],
            product_info['short_url'],
            product_info['views'],
            product_info['favorite_counter'],
            link_to_profile,
            owner_info['prods_active_cnt'],
            owner_info['prods_sold_cnt']
    ]
    return record


def process_url(url, outfile):
    productID = get_productID(url)
    product_info = get_product_info(productID)
    record = prepare_record(product_info)
    write_row(outfile, record)


def main():
    with open("urls.txt") as file:
        with open('output.csv', 'w', newline='') as csv_file:
            write_header(csv_file)
            for count, line in enumerate(file, start=1):
                line = line.strip()
                try:
                    process_url(line, csv_file)
                    print(f"{count} Ok: '{line}'")
                except:
                    print(f"{count} Skip: '{line}'")
                    continue

if __name__ == "__main__":
    main()
