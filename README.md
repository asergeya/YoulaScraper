# Youla Scraper

There are two version. 

## YoulaHtmlParser Version

Requests html page and parse it, but for getting number of sold products uses
Youla API (https://api.youla.io/api/v1/user/).

### Requirements

- Python 3
- pyopenssl
- requests
- bs4

### Installation

1. Install `python3`
2. pip3 install -r requirements.txt 

### Using

## API Version

Uses Youla API (https://api.youla.io/api/v1/product/).

### Requirements

- Python 3

### Installation

1. Install `python3`
