import json
import re
import csv
import time
import socket
import urllib.request
import requests
from requests.exceptions import Timeout
from requests.exceptions import MissingSchema
from requests.exceptions import InvalidSchema
from requests.exceptions import InvalidURL
from bs4 import BeautifulSoup


def write_header(csv_file):
    writer = csv.writer(csv_file)
    writer.writerow(["Наименование", "Ссылка", "Просмотры", "В избранном", "Ссылка на продавца", "Активные", "Проданные"])


def write_row(csv_file, info):
    writer = csv.writer(csv_file)
    writer.writerow(info)


def get_html(url):
    html = ""
    sleep_time = timeout = 5
    retries = 0
    while not html:
        try:
            headers = {
                'User-Agent': 'Mozilla/5.0 (X11; Linux x86_64; rv:96.0) Gecko/20100101 Firefox/96.0',
                'Connection': 'keep-alive',
            }
            html = requests.get(url, timeout=timeout, headers=headers).text
            #time.sleep(10)
            break
        except Timeout as e:
            print(str(e))
            print(f"Sleep for {sleep_time} seconds.")
            time.sleep(sleep_time)
            retries += 1
            if retries > 0:
                sleep_time += 5
            continue
    return html


def parse(url):
    html = get_html(url)
    soup = BeautifulSoup(html, 'html.parser')
    scripts = soup.find_all("script")
    data = re.search('window.__YOULA_STATE__ = (.*);', scripts[2].text)
    
    return json.loads(data.group(1))


def get_link_to_profile(ownerID):
    return f"https://youla.ru/user/{ownerID}"


def get_owner_data(ownerID):
    with urllib.request.urlopen(f"https://api.youla.io/api/v1/user/{ownerID}") as url:
        data = json.loads(url.read().decode())

        return data['data']



def process_url(url, outfile):
    json = parse(url)
    product = json["entities"]["products"][0]
    link = get_link_to_profile(product["owner"]["id"])
    owner_info = get_owner_data(product["owner"]["id"])
    write_row(outfile, [product["name"], url, product["views"], product["favoriteCounter"],
                        link, owner_info["prods_active_cnt"], owner_info["prods_sold_cnt"]])


def main():
    with open("urls.txt") as file:
        with open('output.csv', 'w', newline='') as csv_file:
            write_header(csv_file)
            for count, line in enumerate(file, start=1):
                line = line.strip()
                try:
                    process_url(line, csv_file)
                    print(f"{count} Ok: '{line}'")
                except (MissingSchema, InvalidSchema, InvalidURL, OSError, KeyError, AttributeError) as e:
                    print(str(e))
                    print(f"{count} Skip: '{line}'")


if __name__ == "__main__":
    main()
